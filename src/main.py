# -*- coding: latin-1 -*-

'''
Created on May 6, 2016

@author: richard
'''
import webapp2
import logging
from xlsxwriter.workbook import Workbook
from google.appengine.ext.webapp.util import run_wsgi_app
import io
import sys
import ftplib

class TestExcel(webapp2.RequestHandler):
    
    def get(self):
        output = io.BytesIO()

        # references of xlsxwriter my be founded on: http://xlsxwriter.readthedocs.io/index.html
        
        workbook = Workbook(output, {'in_memory': True})
        worksheet = workbook.add_worksheet()
        worksheet.write(0, 0, 'Hello, world!')
        workbook.close()

        # to finish buffer....
        output.seek(0)
        
        #WRITE to a file in binary mode...
        with io.open('/Volumes/Files/myfile.xlsx', 'wb') as f:
            f.write(output.getvalue())
        
        #close buffer....
        f.close() 
        
        self.response.write('done')
        
class FtpUploader(webapp2.RequestHandler):
    
    def get(self):
        login = "jbsacademia_us"
        passwd = "asghais23456*&#"
        host = "54.243.25.154"
        file1 = None
        ftp = None
      
        try:
            print "Logging in..."
            ftp = ftplib.FTP(host)
            #ftp.set_pasv(False)
            ftp.login(login, passwd)
            #print ftp.retrlines('LIST')
            print 'Logged'
            file1 = open('/Volumes/Files/test1.txt', "rb")
            print ftp.storbinary('STOR test1.txt', file1) 
        except:
            e = sys.exc_info()
            print e
        finally:
            if file1:
                file1.close()
            if ftp:
                ftp.quit()
            


application = webapp2.WSGIApplication([
    webapp2.Route('/test_excel', TestExcel),
    webapp2.Route('/test_ftp', FtpUploader)
   ], debug=True)

def main():
    run_wsgi_app(application)

if __name__ == "__main__":
    main()


logging.getLogger().setLevel(logging.DEBUG)
